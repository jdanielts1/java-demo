package demo.springsample;

import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import demo.springsample.model.Products;
import demo.springsample.repository.ProductRepository;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductManagementApplicationController {

    private final ProductRepository fruitRepository;

    @PostMapping
    private Products addNew(@RequestBody Products fruit) {
        return fruitRepository.save(fruit);
    }

    @PutMapping("/{id}")
    private Products addNew(@PathVariable Long id, @RequestBody Products fruit) {
        var existing = fruitRepository.findById(id).orElseThrow(() -> new RuntimeException("Record not found"));
        existing.setName(fruit.getName());
        existing.setColor(fruit.getColor());
        return fruitRepository.save(existing);
    }

    @GetMapping
    public Page<Products> getPaginatedResponse(@RequestParam int pageNumber, @RequestParam int pageSize) {
        return fruitRepository.findAll(PageRequest.of(pageNumber, pageSize));
    }
    @GetMapping("/health")
    public Timestamp serviceUnavailable() {
    	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timestamp);
        return timestamp;
    }
}
